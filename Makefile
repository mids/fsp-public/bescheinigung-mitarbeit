.PHONY: default
default: all

.PHONY: all
all: build

.PHONY: cleantmpfiles
cleantmpfiles:
	rm -f *.log *.out *.aux *.toc *.markdown.in *.markdown.lua *.fdb_latexmk *.fls log

.PHONY: clean
clean: cleantmpfiles
	rm -f *.pdf

.PHONY: build
build:
	pdflatex --shell-escape bescheinigung.tex
